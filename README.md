# 有宠商城App

#### 前言

愿你在迷茫时，坚信你的珍贵，爱你所爱，行你所行，听从你的心，无问西东。

#### 项目介绍

之前是有发过这个项目的，当时这个项目的引导页已经做得非常不好，功能虽然已经实现，但是bug极多，很多小伙伴给我留言说让我改进一下，后来抽了个时间就改进了一些东西：
1. 封装了一些BaseActivity和BaseFragment，当时写这个项目没有这些思想，新页面就是单独的Activity和Fragment,冗余的代码数不胜数，毫无封装概念;
2. 改进了引导页的四个动画，利用RecyclerView去实现ViewPager，一次只能翻一页，并且滑动停止开始加载动画，动画的加载也没有采用帧动画，毕竟120多张图，帧动画谁用谁知道Out of memory，后来Google了一下，选择SurfaceView去实现这个动画，结果非常完美和流畅，有兴趣的朋友可以去试着玩儿一下;
3. 多个页面的Banner改进，之前使用Handler去实现的，当时想着是自己去体会一下Handler的用法，后来了解了内存泄漏这个东西后，还是采用了比较成熟的三方框架[Banner](https://github.com/youth5201314/banner)。

#### 项目环境和架构

1. 开发环境Android Studio3.2.1 Gradle4.6;
2. 整个项目采用MVP和MVC混用的开发模式，毕竟一些小功能页面没必要新增几个对象;
3. 未采用RxJava等热门开发框架，有兴趣的小伙伴可以去试着实现;

#### 界面预览

1. 启动页<br/>
![](https://app-screenshot.pgyer.com/image/view/app_screenshots/d63561df310181d3601c5dca7eebb329-528)
2. 导航页<br/>
![](https://app-screenshot.pgyer.com/image/view/app_screenshots/b03474a43f3be27fe543be6036898a9a-528)
3. 登录<br/>
![](https://app-screenshot.pgyer.com/image/view/app_screenshots/9ea252602210431a7c375470ad05a2b4-528)
4. 主页面<br/>
![](https://app-screenshot.pgyer.com/image/view/app_screenshots/8d2d6d512df012a9ba0186d2f45456d4-528)
5. 商城<br/>
![](https://app-screenshot.pgyer.com/image/view/app_screenshots/82481c53ea5eb6f8f1f53f9ee9d1973d-528)

#### APK安装地址

![](https://www.pgyer.com/app/qrcode/Wei_Pet)

#### 总结

1. 项目地址：https://gitee.com/YiDer/pet_version_1.0
2. 码云个人地址： https://gitee.com/YiDer
3. 联系方式：1070138445
4. 如果有好的想法，或者在开发的道路上遇到什么问题可以随时联系我~
5. 广告：本人承接各个学历的计算机毕业设计，主要方向是Java，Android以及Web，公司信誉和个人资料担保，开发周期固定，保障顾客利益，避免各类骗术！